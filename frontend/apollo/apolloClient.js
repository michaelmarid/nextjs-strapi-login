// libs
import { withApollo } from 'next-apollo';
import ApolloClient, { InMemoryCache } from 'apollo-boost';
import Cookie from "js-cookie";

// graphql api
const API_URL = process.env.NEXT_PUBLIC_API_URL;

// create the client
const apolloClient = new ApolloClient({
    uri: `${API_URL}/graphql`,
    cache: new InMemoryCache(),
    request: (operation) => {
        const token = Cookie.get("token");
        if (token) {
            operation.setContext({
                headers: {
                    Authorization: token ? `Bearer ${token}` : "",
                  }
            })
        }      
    }
});

// export customized client
export default withApollo(apolloClient);