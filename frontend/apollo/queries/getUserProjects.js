// libs
import { gql } from '@apollo/client';

// query
const GET_USER_PROJECTS = gql`
    query GET_USER_PROJECTS($user: ID!) {
        userProjects(where: { user: $user}) {
  	        id
            title
            description
            linkImage
            category
	    }
    }
`;

// export
export default GET_USER_PROJECTS;