// libs
import React, { useContext } from 'react';
import { Layout, Row, Col, Tag, Button } from 'antd';
import {
    GitlabOutlined,
    LinkedinOutlined,
    LogoutOutlined,
    PoweroffOutlined
} from '@ant-design/icons';
import Router from "next/router";
import Cookie from "js-cookie";

// context
import AppContext from '../../context/AppContext';

// main
const UserLayout = ({ children }) => {

    // context
    const { setUser } = useContext(AppContext);

    // ant design
    const { Header, Content, Footer } = Layout;

    // exit method
    const logOff = () => {

        // remove the cookie token
        Cookie.remove("token");
        // sync logout between multiple windows
        window.localStorage.setItem("logout", Date.now());
        // disable user
        setUser(null);
        // redirect to the home page
        Router.push("/");
        // last case
        location.reload();
    }

    // render
    return (
        <Layout style={{overflow: 'auto', minHeight: '100vh'}}>

            <Layout className="site-layout">
                <Header className="site-main header">
                    {/*<Link href="/user/home">
                        <a><img src="#" alt="logo" className="logo" /></a>
                    </Link>*/}
                    <span className="fund-title">DEMO SITE</span>
                    <Button icon={<PoweroffOutlined />} onClick={() => logOff()} danger type="primary" shape="circle" />
                </Header>
                <Content className="site-main content">
                    {children}
                </Content>
                <Footer className="site-main footer">
                    <Row>
                        <Col span={24} style={{textAlign: 'center'}}>
                            <p>{process.env.NEXT_PUBLIC_COMPANY}</p>
                            <p>
                                <a href="https://gitlab.com/michaelmarid/live-projects" target="_blank">
                                    <Tag icon={<GitlabOutlined />} color="red">GitLab</Tag>
                                </a>
                                <a href="https://gr.linkedin.com/in/%CE%BC%CE%B9%CF%87%CE%B1%CE%AE%CE%BB-%CE%BC%CE%B1%CF%81%CE%B9%CE%B4%CE%AC%CE%BA%CE%B7%CF%82-3bb09782?trk=people-guest_people_search-card" target="_blank">
                                    <Tag icon={<LinkedinOutlined />} color="blue">LinkedIn</Tag>
                                </a>
                            </p>
                            <p>Powered by Michail Maridakis</p>
                        </Col>
                    </Row>
                </Footer>
            </Layout>
            <style jsx>
            {`
              .logo {
                margin: 12px;
                width: 250px;
              } 
              .site-main {
                background: #fff;
              }
              .header {
                padding: 0px 20px;
                height: 100px;
                background-color: #000000;
                background-image: linear-gradient(0deg, #000000 0%, #b82e1f 100%);
              }
              .content {
                padding: 20px;
                background-color: #000000;
                color: white;
              }
              .content h1 {
                color: white;
              }
              .footer {
                background-color: #000000;
                background-image: linear-gradient(180deg, #000000 0%, #b82e1f 100%);
                padding: 5px;
                color: #000000;
              }
              .fund-title {
                  margin-left: 30px;
                  margin-right: 30px;
                  font-weight: bold;
                  font-size: 120%;
                  color: black;
              }
              @media only screen and (max-width: 900px) {
                    .header {
                        padding: 0px;
                        height: 120px;
                        line-height: 20px;
                    }
                    .logo {
                        width: 100px;
                        margin: 0px;
                    }
                    .fund-title {
                        float: left;
                        font-size: 90%;
                    }
              }
            `}
            </style>
        </Layout>
    );

}

// export
export default UserLayout;