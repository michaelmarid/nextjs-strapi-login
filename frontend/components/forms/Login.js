// libs
import React, { useState } from 'react';
import { Form, Input, Button, Alert } from 'antd';
import Cookie from "js-cookie";

// backend
const API_URL = process.env.NEXT_PUBLIC_API_URL;

// styles
import loginStyle from '../../styles/login.module.css';

// main
const Login = ({ changeView }) => {

    // state
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    // form
    const onFinish = (values) => {

        // console.log(values);

        // erase any error messages and show the loading index
        setError(false);
        setLoading(true);

        // prevent function from being ran on the server
        if (typeof window === "undefined") return;

        // login promise
        const loginPromise = new Promise((resolve, reject) => {

            // create http request with data
            const request = new XMLHttpRequest();

            // send the request to the backend
            request.open('POST', `${API_URL}/auth/local/`);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify({
                identifier: values.email,
                password: values.password
            }));

            // good responses
            request.onload = function() {
                // error state
                if (request.status != 200) reject(request);
                // resolve
                else {
                    // set cookie
                    let res = JSON.parse(request.response);
                    Cookie.set("token", res.jwt, { 
                        sameSite: 'Lax',
                        expires: 1
                    });
                    // return resolved
                    resolve();
                }  
            }
            // bad response
            request.onerror = function() {
                reject(request);
            };

        });

        // return the relative promise
        return loginPromise
            .then((resolved) => {
                // ok scenario: reload (with cookie)
                setLoading(false);
                location.reload();
            })
            .catch((error) => {
                // console.log(error);
                // error messages
                if (error.response) {
                    let messageObj = JSON.parse(error.response);
                    if (messageObj && messageObj.message && messageObj.message.length && messageObj.message[0].messages) {
                        setError(messageObj.message[0].messages[0].message);
                    }
                    else setError('Server Problem');
                }
                else setError('Server Problem');
                setLoading(false);
        });

    } // onFinish
    
    
    // render
    return (
        <div className={loginStyle.content}>

            <h1 className={loginStyle.title}>Login</h1>

            <Form name="loginForm" onFinish={onFinish}>
                <Form.Item 
                    name="email"
                    rules={[
                        { required: true, message: 'Email is required!' },
                        { type: 'email', message: 'Invalid Email!'}
                    ]}
                    hasFeedback
                ><Input placeholder="Email" className={loginStyle.field} /></Form.Item>

                <Form.Item
                    name="password"
                    rules={[
                        { required: true, message: 'Please input your password!' }
                    ]}
                    hasFeedback
                    style={{width: '100%'}}
                ><Input.Password placeholder="Password" className={loginStyle.field} /></Form.Item>

                <Form.Item>
                    <Button type="link" htmlType="button" onClick={() => changeView('forgot')} className={loginStyle.signup}>Password Reset?</Button>
                    <br /><br /><br /><br />
                    <Button type="primary" htmlType="submit" loading={loading} block className={loginStyle.submit}>login</Button>
                </Form.Item>

            </Form>

            {
                error && 
                    <Alert
                      key={'error'}
                      type="error"
                      message="Error"
                      description={error}
                      showIcon
                      closable
                    />
              }

        </div>
    );

}

/********************* ΕΞΑΓΩΓΗ ***********************/
export default Login;