// libs
import React, { useState } from 'react';
import { Form, Input, Button, Alert } from 'antd';

// styles
import loginStyle from '../../styles/login.module.css';

// api
const API_URL = process.env.NEXT_PUBLIC_API_URL;

// main
const Forgot = ({ changeView }) => {

    // state
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [message, setMessage] = useState(false);

    // form submission
    const [form] = Form.useForm();

    const onFinish = (values) => {

        // console.log(values);

        // reset
        setError(false);
        setMessage(false);
        setLoading(true);

        // prevent function from being ran on the server
        if (typeof window === "undefined") return;

        // reset promise
        const resetPromise = new Promise((resolve, reject) => {

            // create http request with data
            const request = new XMLHttpRequest();

            // send the request to the backend
            request.open('POST', `${API_URL}/auth/forgot-password`);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify({
                email: values.email
            }));

            // good responses
            request.onload = function() {
                // error state
                if (request.status != 200) reject(request);
                // resolve
                else {
                    // return resolved
                    resolve();
                }  
            }
            // bad response
            request.onerror = function() {
                reject(request);
            };

        });

        // return the relative promise
        return resetPromise
            .then((resolved) => {
                // ok scenario: reload (with cookie)
                setLoading(false);
                form.resetFields();
                setMessage(true);
            })
            .catch((error) => {
                // console.log(error);
                // error messages
                if (error.response) {
                    let messageObj = JSON.parse(error.response);
                    if (messageObj && messageObj.message && messageObj.message.length && messageObj.message[0].messages) {
                        setError(messageObj.message[0].messages[0].message);
                    }
                    else setError('Server Problem');
                }
                else setError('Server Problem');
                setLoading(false);
        });

            
    } // on finish
    
    
    // view
    return (
        <div className={loginStyle.content}>

            <h1 className={loginStyle.title}>{process.env.NEXT_PUBLIC_FUND}</h1>
            <p className={loginStyle.subtitle}>Please, use your email address in order to get a link to reset your password:</p>

            <Form name="forgotForm" onFinish={onFinish} form={form}>
                <Form.Item 
                    name="email"
                    rules={[
                        { required: true, message: 'Email is required!' },
                        { type: 'email', message: 'Invalid Email!'}
                    ]}
                    hasFeedback>
                    <Input placeholder="Email" className={loginStyle.field} />
                </Form.Item>
                <Form.Item 
                    name="email-confirmation"
                    rules={[
                        ({ getFieldValue }) => ({ validator(rule, value) {
                            if (!value || getFieldValue('email') === value) { return Promise.resolve(); }
                            return Promise.reject("Email addresses don't match!");
                        }})
                    ]}
                    hasFeedback>
                    <Input placeholder="Email Confirmation" className={loginStyle.field} />
                </Form.Item>
                <br />
                <Form.Item>
                    <Button type="primary" htmlType="submit" loading={loading} block className={loginStyle.submit}>send link</Button>
                    <br /><br />
                    <Button type="link" htmlType="button" onClick={() => changeView('login')} className={loginStyle.signup}>return</Button>
                </Form.Item>

            </Form>

            {
                error && 
                    <Alert
                      key={'error'}
                      type="error"
                      message="Error"
                      description={error}
                      showIcon
                      closable
                    />
              }
      
              {
                message ? (
                  <Alert
                    type="success"
                    message="Password Reset"
                    description="You will soon get a new email with guidelines in order to reset your password."
                    showIcon
                  />
                ): undefined
              }
        </div>
    );

}

/********************* ΕΞΑΓΩΓΗ ***********************/
export default Forgot;