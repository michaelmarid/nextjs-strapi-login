// libs
import React, { useContext, useEffect } from 'react';
import { useRouter } from 'next/router';

// app (auth) context
import AppContext from '../../context/AppContext';

// protected route
export function ProtectedRoute(Component) {
    return () => {
        // context
        const { isAuthenticated, loading } = useContext(AppContext);
        // router
        const router = useRouter();
        // on mount
        useEffect(() => {
            if (!loading && !isAuthenticated) router.push('/');
        }, [isAuthenticated, loading] );
        // temporary view (if user from context not ready)
        if (loading) return <div>Loading...</div>;
        // return HOC
        return (
            isAuthenticated ? <Component {...arguments} /> : <div>Loading...</div>  
        );
  
    }
}