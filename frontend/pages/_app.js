// libs
import React, { useEffect, useState } from 'react';
import Head from "next/head";
import 'antd/dist/antd.css';
import Cookie from "js-cookie";
import fetch from "isomorphic-fetch";

// apollo client
import withApollo from '../apollo/apolloClient';

// context
import AppContext from '../context/AppContext';

// app component
const Main = ({ Component, pageProps }) => {

    // state
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    // component did mount
    useEffect(() => {

      // check if there is a token
      const token = Cookie.get("token");

      // if there is a token (after user login)
      if (token) {
          // authenticate the token on the server and place set user object
          fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/me`, {
              headers: { Authorization: `Bearer ${token}` }
          }).then(async(res) => {
              
              // if status is not 200-OK
              if (!res.ok) {
                  // delete the cookie and the user from the state
                  Cookie.remove("token");
                  setUser(null);
                  setLoading(false);
                  return null;
              }
              // if status is 200-ok get the response json and save it to state
              const user = await res.json();
              // console.log("User in _app.js:", user);
              setUser(user);
              setLoading(false);
          });
      }
      // if no token nothing more to wait
      else setLoading(false);
  }, []);
    
    // common render for all pages
    return (
      <AppContext.Provider
        value={{
          user,
          setUser,
          loading,
          isAuthenticated: !!user, // evaluates to true if there is a user,
        }}
      >
        { /* header */}
        <Head>
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <title>Demo Site</title>
        </Head>
        {/* body */}
        <Component {...pageProps} />
        {/* close app provider */}  
      </AppContext.Provider>  
    );
}

// export with ssr apollo
export default withApollo({ ssr: true })(Main);