// libs
import React, { useContext } from 'react';
import { Spin, Carousel, Avatar } from 'antd';
import { useQuery } from '@apollo/client';

// graphql
import GET_USER_PROJECTS from '../../apollo/queries/getUserProjects';

// context
import AppContext from '../../context/AppContext';

// components
import { ProtectedRoute } from '../../components/auth/ProtectedRoute';

// styles
import UserLayout from '../../components/layouts/UserLayout';
import { urlObjectKeys } from 'next/dist/next-server/lib/utils';

// main
const Home = () => {

  // context
  const { user } = useContext(AppContext);

  // graphql
  const { loading, error, data } = useQuery(GET_USER_PROJECTS, { variables: { user: user.id } });
  if (loading) return <Spin size="large" />;
  if (error) return <p>Server Error</p>;
  // console.log(data);

  // carousel style
  const contentStyle = {
    height: '50vh',
    color: '#fff',
    lineHeight: '100px',
    textAlign: 'center'
  };

  // render
  return (
    <UserLayout>
        <Carousel dotPosition={'left'}>
          { data && data.userProjects && data.userProjects.length === 0 && (
            <div style={{height: '100%'}}><h1 style={{color: 'white'}}>NO PROJECTS YET</h1></div>
          )}
          { data && data.userProjects && data.userProjects.length > 0 && (
            data.userProjects.map((project, idx) => {
              return (
                <div key={idx}>
                  <h3 style={contentStyle}>
                    <span style={{fontSize: '1.4rem'}}>{project.title}</span>
                    <br />
                    <Avatar shape="square" size={200} src={project.linkImage} />
                    <br />
                    <span style={{fontSize: '1.1rem'}}>{project.description}</span>
                    <br />
                    
                  </h3>
                </div>
              )
            })
          )}
        </Carousel>  
    </UserLayout> 
  );
}

// export
export default ProtectedRoute(Home);