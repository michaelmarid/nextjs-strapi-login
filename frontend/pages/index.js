// libs
import React, { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

// context
import AppContext from '../context/AppContext';

// components
import Login from '../components/forms/Login';
import Forgot from '../components/forms/Forgot';
// import Register from '../components/forms/Register';

// styles
import loginStyle from '../styles/login.module.css';

// main
const Home = () => {

  // state
  const [view, setView] = useState('login');

  // context
  const { isAuthenticated } = useContext(AppContext);

  // rooter
  const router = useRouter();

  // valid user roote
  useEffect(() => {
    if (isAuthenticated) router.push('/user/home');
  }, [isAuthenticated]);

  // temporary view
  if (isAuthenticated) return <div>Loading...</div>

  // render
  return (
    <div className={loginStyle.loginwrapper}>
      { view === 'login' && <Login changeView={setView} /> }
      { view === 'forgot' && <Forgot changeView={setView}/> }
    </div> 
  );
}

// export
export default Home;